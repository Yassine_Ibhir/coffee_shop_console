package eclipse.main;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import eclipse.authentication.Authentication;
import eclipse.shop.Ingredients;
import eclipse.shop.Product;
import eclipse.shop.SelectedProducts;
import eclipse.shop.ShoppingCart;
import eclipse.shop.Stock;

public class CoffeZone {

	private static Authentication register ;

	private static Authentication login  ;

	public  static Connection conn;
	
	//  Collections of  objects.

	private static ArrayList<Product> menu = new ArrayList<Product>() ;

	private static ArrayList<SelectedProducts> cart = new ArrayList<SelectedProducts>();
	
	public static ArrayList<Stock> ingr_stock = new ArrayList<Stock>();
	
	// shoppingCart or product_sales
	private static ShoppingCart order = new ShoppingCart(cart);

	public static void main(String[] args) throws SQLException {

		try{
			String url = "jdbc:oracle:thin:@localhost:1521:XE"; //local URL
			conn = DriverManager.getConnection(url, "system", "Flous101992");	
		}catch(SQLException e) {
			System.out.println("Failed to connect: try again later.");
		}
		
		openCoffeeShop();
		conn.close();
	}
	
	/**
	 * Uses helper methods to set up products, ingredients and stock.
	 * allowing user to choose between registration,
	 *  logging and viewing products
	 * @return boolean false when client decide to checkout
	 */
	private static void openCoffeeShop() {
		
		prepareProducts();
		prepareStock();
		
		boolean isCheckout = false;
		
		while(!isCheckout) {
			int choice = homePage();
			if(choice == 1) {
				isCheckout = logWhenRegistered();
			}
			else if (choice == 2) {
				isCheckout = logInToContinue();			
			}
			else {
				viewProducts();
			}
		}
	}
	// Allow the user to choose the service
		public static int homePage() {

			boolean isValid = false;
			int service = 0;

			while(!isValid) {

				Scanner reader = new Scanner(System.in);

				System.out.println("Welcome to Coffee Zone... \n"
						+ "To register enter 1. \n"
						+ "To login enter 2.\n"
						+ "To view products enter 3.");
				try {
					service = reader.nextInt();
					if(service > 0 && service<4) {
						isValid = true;
					}
				}
				catch(InputMismatchException e) {
					System.out.println("Enter a valid number");
				}	
			}
			return service;
		}
		/**
		 * log in the customer when the registration is successful
		 * @return 
		 */
		public static boolean logWhenRegistered() {
			
			boolean isCheckout = false;
			if (registerClient()) {
				login =  new Authentication(register.getUname(),register.getPassword());
				System.out.println("Now you can order Mr. "+ login.getUname());
				isCheckout = manageProducts();
			}
			else {
				System.out.println("You are not registered: Try again..");
			}
			return isCheckout;
		}


	/**
	 * Allows the user to log in and place orders
	 * @return boolean false when client decide to checkout
	 */
	public static boolean logInToContinue() {
		
		boolean isCheckout  = false;	
		loginClient(); //instiantiate a login object		
		if(login.isLogin()) {
			login.addLogInfo();
			System.out.println("Now you can order Mr. "+ login.getUname());
			isCheckout = manageProducts();
		}
		else {
			System.out.println("You are not logged in: Try again..");
		}
		
		return isCheckout;
	}
	

	
	//Choose products and quantity: Main menu
	private static boolean manageProducts() {

		boolean isCheckout = false;

		while(!isCheckout) {
			System.out.println("This is the Menu:");
			viewProducts();
			Scanner select = new Scanner(System.in);
			try {
				System.out.println("To Select a product enter 1. \n"
						+ "To checkout enter 2,\n"
						+ "To modify an order enter 3.");

				int choice = select.nextInt();

				switch(choice) {
				case 1:					
					selectProduct();
					break;
				case 2:
					isCheckout = order.finalizedPurchase(login.getUname());
					break;
				case 3:
					if(order.getCart().size()>0) {
						//ShoppingCart
						order.modifyCart();
					}

					else {
						System.out.println("you don't have any product in your cart");
					}

					break;
				default:
					System.out.println("enter a valid number");
					break;
				}
			}
			catch(InputMismatchException e) {
				System.out.println("enter a valid number");				
			}	
		}
		return isCheckout;
	}

	// add product to the cart arrayList
	private static void selectProduct() {

		try {				
			Scanner add = new Scanner(System.in);
			System.out.println("Choose the menu number and quantity:");
			System.out.println("Enter the Menu#:");					
			int menuNum = add.nextInt();
			if(menuNum>0 && menuNum<5) {
				System.out.println("Enter quantity:");
				int quantity = add.nextInt();
				if( quantity>0 &&  isIngredienttAvailable(menuNum,quantity)) {
					addProductToCart(menuNum,quantity);
				}
				else {
					System.out.println("Quantity is not valid or product is out of Stock; try again");
				}
			}
			else {
				System.out.println("Choose a valid menu number");	
			}
		}
		catch(InputMismatchException e) {
			System.out.println("enter a valid number; Start again");	
		}						
	}

	// checks if there are enough ingredients to make the product
	public static boolean isIngredienttAvailable(int product_id,int quantity) {

		for(Product product : menu) {
			if(product.getMenuNum() == product_id) {
				return product.isProductAvailable( quantity);	
			}
		}

		return false;
	}


	// addProduct to the cart or modify quantity if the product already exist
	private static void addProductToCart(int menuNum,int quantity) {
		
		SelectedProducts productToAdd = null;
		for (Product p : menu) {
			if(p.getMenuNum() == menuNum) {
				productToAdd = order.menuExist(menuNum);
				if(productToAdd == null) {
					SelectedProducts sp = new SelectedProducts(p.getMenuNum(),
							p.getName(),p.getPrice(),quantity,p.getIngredients());
					cart.add(sp);
					System.out.println("Product added to your Cart:");		
					order.setCart(cart);
					System.out.println(order.toString());
				}
				else {
					productToAdd.setQuantity(quantity+productToAdd.getQuantity());
					System.out.println("Product added to your Cart:");
					System.out.println(order.toString());
				}
				
			}
		}
	}
	// store the products of the coffee shop inside the arraylist products.
	private static void prepareProducts() {
		String pro = "SELECT * FROM products";
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(pro);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				ArrayList<Ingredients> p_ingr = prepareIngredients(rs.getInt(1));
				Product p = new Product(rs.getInt(1),rs.getString(2),rs.getDouble(3),p_ingr);
				menu.add(p);
			}
		} catch (SQLException e) {
			System.out.println("Can not view products, try again later..");
		}
		finally {
		    if (ps != null) {
		    	try {
					ps.close();
				} catch (SQLException e) {
					System.out.println("we are having problems with the database Connection...");
				}
		    }
		}
	}
	
	
	// get the ingredients of each product, set those ingredients to be part of the product object.
	private static ArrayList<Ingredients> prepareIngredients(int ing_id) {	
		
		String q = "select ingr_id , ingr_quantity from product_ingredients\r\n" + 
				"where product_id = ?";

			// This array will be part of every product
			ArrayList<Ingredients> p_ingr = new ArrayList<Ingredients>();
			PreparedStatement ps = null;
			try {
			    ps = conn.prepareStatement(q);
				ps.setInt(1, ing_id);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					Ingredients ing = new Ingredients(rs.getInt("ingr_id"),rs.getInt("ingr_quantity"));
					p_ingr.add(ing);
				}
			
			} catch (SQLException e) {
				System.out.println("sql exception: Try again");
			}
			finally {
			    if (ps != null) {
			    	try {
						ps.close();
					} catch (SQLException e) {
						System.out.println("we are having problems with the database Connection...");
					}
			    }
			}
		
		return p_ingr;
		
	}
	
	// store each ingredient and the stock value in the ArrayList<Stock>
	private static void prepareStock() {
		
		String q = "select ingr_id , stock from ingredients";
	
		PreparedStatement ps =  null;
		try {
			ps = conn.prepareStatement(q);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Stock s = new Stock(rs.getInt(1),rs.getInt(2));
				ingr_stock.add(s);
			} 
		}
		catch (SQLException e) {
			System.out.println("connection problems...Try again later");
		}
		finally {
		    if (ps != null) {
		    	try {
					ps.close();
				} catch (SQLException e) {
					System.out.println("we are having problems with the database Connection...");
				}
		    }
		}
	
	}
	

	//display all products.
	private static void viewProducts() {
		for(Product p : menu) {
			System.out.println(p.toString());
		}
	}


	// ask the user for the infotmation to login
	private static void loginClient() {

		Scanner log = new Scanner(System.in);
		System.out.println("enter username:");
		String username = log.nextLine();
		System.out.println("enter password:");
		String psw = log.nextLine();
		login =  new Authentication(username,psw);

	}

	//ask the user for all necessary information to register
	private static boolean registerClient() {

		boolean registered = false;
		Scanner reg = new Scanner(System.in);
		while(!registered) {

			System.out.println("Enter username:");
			String username = reg.nextLine();
			System.out.println("Enter password:");
			String psw =  reg.nextLine();
			System.out.println("Enter referral username if you know it otherwise type anything :");
			String refferalUserName =  reg.nextLine();
			System.out.println("Enter Your address:");
			String address =  reg.nextLine();
			try {
				register = new Authentication(username,psw,refferalUserName,address);
				registered = register.addNewCustomer();
				checkCustomerDiscount(username);
			}
			catch(IllegalArgumentException e) {
				System.out.println("One of your credentials is not valid");
			}
		}
		return 	registered;
	}
	
	
	/**
	 * method will use a helper method to add bonus if the new customer
	 * was referred by another customer
	 * @param username
	 */
	private static void checkCustomerDiscount(String username) {
		
		String q = "select referral from clients where user_name = ?";
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(q);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				int referral = rs.getInt("referral");
				
				System.out.println("ref: "+ referral);
				if(referral != 0) {
					//if the referal id exist then add 5 bonus to the customer dicount field
					setCustomerDiscount(referral,5);
				}
			} 
		}
		catch (SQLException e) {
			System.out.println("we are having problems with the database Connection...");
		}
		finally {
		    if (ps != null) {
		    	try {
					ps.close();
				} catch (SQLException e) {
					System.out.println("we are having problems with the database Connection...");
				}
		    }
		}
	}
	
	// set the discount to be 5 
	//for the customer that referred the logged in customer
	public static void setCustomerDiscount(int referral,double amount) {
		
		int cusid = 0;
		String q = "{ call customer_authentication.set_bonus(?,?)}";
		CallableStatement cstm = null;
		try {
			cstm = conn.prepareCall(q);
			cstm.setInt(1, referral);
			cstm.setDouble(2, amount);
			cstm.executeUpdate();
		} catch (SQLException e) {
			System.out.println("we are having problems with the database Connection..");
		}
		
		finally {
		    if (cstm != null) {
		    	try {
					cstm.close();
				} catch (SQLException e) {
					System.out.println("we are having problems with the database Connection..");
				}
		    }
		}
		
	}
}

