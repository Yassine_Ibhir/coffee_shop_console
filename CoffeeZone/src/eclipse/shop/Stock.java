package eclipse.shop;

import java.util.ArrayList;

import eclipse.main.CoffeZone;

public class Stock {
	private int ingredient;
	private int stock;
	
	
	// constructor to initialize the stock object
	public Stock(int ingredient,int stock) {
		this.ingredient = ingredient;
		this.stock = stock;
	}
	// getters
	public int getIngredient() {
		return this.ingredient;
	}
	public int getStock() {
		return this.stock;
	}

	//setter
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	/* This method will remove or add the stock.
	 * We are modifying the value of the stock everytime
	 * The user add, modifie or remove a product this means
	 * that the quantity could be negative or positive.
	 */
	public static void changeStockState(int quantity,
			ArrayList<Ingredients> ingreds) {
		
		ArrayList<Stock> ingStock = CoffeZone.ingr_stock;
		
		for(Ingredients ing : ingreds) {
			for(Stock s : ingStock) {
				if(ing.getIngr() == s.getIngredient()) {
					s.setStock(Math.abs(quantity*ing.getQ()-s.getStock()));
				}
			}
		}
	
	}
}