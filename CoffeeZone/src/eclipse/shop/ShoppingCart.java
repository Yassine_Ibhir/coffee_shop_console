package eclipse.shop;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import eclipse.main.CoffeZone;

/**
 * This class will Store, modify and remove the products
 *  selected by the  customer.
 * @author Yassine Ibhir
 *
 */
public class ShoppingCart {


	private ArrayList<SelectedProducts> cart = new ArrayList<SelectedProducts>();

	// constructor
	public ShoppingCart(ArrayList<SelectedProducts> cart) {
		this.cart = cart;
	}

	public ArrayList<SelectedProducts> getCart(){
		return this.cart;
	}

	public void setCart(ArrayList<SelectedProducts> cart) {
		this.cart = cart;
	}

	/**
	 *  ask the user if wants to remove product or modify quantity
	 */
	public void modifyCart() {
		Scanner modify = new Scanner(System.in);
		boolean modified = false;
		if(cart.size() < 1) {
			System.out.println("Your cart is empty. Go add some products...");
			modified = true;
		}

		while(!modified) {

			try {
				System.out.println("To modify your cart enter 1. \n"
						+ "To remove a product enter 2.\n"
						+ "To go back enter 3.");

				int choice = modify.nextInt();

				switch(choice) {
				case 1:
					modifyQuantity();
					break;
				case 2:
					removeProduct();
					break;
				case 3:
					modified = true;
					break;
				default:
					System.out.println("Enter a valid number");	
				}

			}
			catch(InputMismatchException e) {
				System.out.println("enter a valid number");	
			}
		}	
	}	

	/**
	 * ask user which product to modify with new quantity
	 */
	private void modifyQuantity() {

		Scanner reader = new Scanner(System.in);

		try {
			System.out.println("This is you Cart: \n "+this.toString());
			System.out.println("Enter menu# you want to modify: ");
			int choice = reader.nextInt();

			if(choice>0 && choice<5) {
				System.out.println("Enter the new quantity: ");
				int quant = reader.nextInt();
				if(quant>0) {
					modify(choice,quant);
				}

				else {
					System.out.println("Not a valid Quantity, Try again...");				
				}
			}

			else {
				System.out.println("Please Enter a valid menu# next time...");
			}
		}
		catch(InputMismatchException e) {
			System.out.println("Not a valid menu#, Try again...");				
		}	
	}

	/**
	 * use helper methods to modify the product
	 * @param choice product_id
	 * @param quant of product
	 */
	private void modify(int choice,int quant) {

		boolean modified = false;

		SelectedProducts sp = findProduct(choice);

		if(sp != null) {

			modified = canBeModified(sp,quant);

			if(modified) {
				sp.setQuantity(quant);
				System.out.println("Menu#: "+ choice+" is modified");
				System.out.println("This is you Cart: \n"+this.toString());

			}
			else {
				System.out.println("Menu#: "+ choice+" can not be modified. Not enough ingredients");
				System.out.println("This is you Cart:\n"+this.toString());
			}
		}
		else {
			System.out.println("Menu#: "+ choice+" does not exist in your cart");
			System.out.println("This is you Cart:\n"+this.toString());
		}

	}

	/**
	 *  This method will use helper methods to determine 
	 *  if the product can be made with the new quantity
	 *  it will be used when customer select,
	 *  modify or remove a product
	 * @param sp Selectedproduct to modify
	 * @param quant new quantity
	 * @return true if there are enough ingredients
	 */
	private boolean canBeModified(SelectedProducts sp, int quant) {

		// gap will store the difference between new quantity and current quantity
		int gap = quant - sp.getQuantity();

		// if difference is >0 meaning newQ is bigger than currenttQ
		// then we need to check if we have enough ingredients
		if(gap>0) {
			return sp.isProductAvailable(gap);
		}
		//here we don't have to check if product is available
		// since newQ is less than currentQ
		// we just change the state of stock.
		if(gap<0) {
			Stock.changeStockState(gap,sp.getIngredients());
			return true;
		}
		// we don't want to do anything here. newQ=currentQ
		return true;
	}

	/**
	 *  Given a menu#, method returns the product object
	 * @param menuNum
	 * @return product with the menuNum
	 */
	private SelectedProducts findProduct(int menuNum) {

		for(SelectedProducts sp : cart) {
			if(sp.getMenuNum() == menuNum) {
				return sp;
			}
		}
		return null;
	}

	/**
	 *  Ask user for menu# To remove
	 */
	private void removeProduct() {

		Scanner remove = new Scanner(System.in);

		boolean removed = false;

		System.out.println("This is you Cart:\n"+this.toString());

		System.out.println("Enter menu# of product you want to remove ");
		try {
			int choice = remove.nextInt();
			if(choice>0 && choice<5) {
				if(isRemoved(choice)) {
					System.out.println("Product removed:");
					System.out.println(cart.size()>0 ? "This is you Cart:\n"+this.toString() : "Cart is empty Now...");
				}
			}
			else {
				System.out.println("Not a valid menu#, Try again...");
			}
		}
		catch(InputMismatchException e) {
			System.out.println("Not a valid menu#, Try again...");
		}		
	}

	/**
	 * removes the product from cart
	 * @param menuNum
	 * @return true if removed else false
	 */
	private boolean isRemoved(int menuNum) {

		boolean removed = false;

		SelectedProducts sp = findProduct(menuNum);

		if(sp != null) {
			//store back quantity of ingredient in stock obj
			removed = canBeModified(sp, 0);
			cart.remove(sp);

		}
		else {
			System.out.println("Menu# "+ menuNum + " is Not in your cart");
		}
		return removed;
	}
	/**
	 * Display information about the order and 
	 * use helper methods to 
	 * ask user to confirm, cancel or continue purchasing
	 * @param user
	 * @return true if customer wants to checkout
	 */
	public boolean finalizedPurchase(String user) {

		boolean finished = isFinished();
		
		if(finished && cart.size()>0) {
			String addrs = getAddress(user);
			String newAddrs = shippingAddress(addrs);
			createOrder(user,newAddrs);
			showBill(user);
//			if(sale_id != 0) {
//				if(insertOrderData(sale_id)) {  
//					showBill(user);
//				}
//			}	
	 }
		else {
			System.out.println("Nothing to order...BYE");
		}
		return  finished;		
	}

	/**
	 * This method will use helper methods to either confirm the order
	 * cancel or continue.
	 * @param user  name
	 * @return
	 */
	private boolean isFinished() {

		boolean finished = false;

		Scanner finalized = new Scanner(System.in);

		try {
			System.out.println("To place the order enter 1 \n  To cancel enter 2 \n  To go back to the main Menu enter 3. ");
			int choice = finalized.nextInt();
			if(choice == 1) {
				finished =  true;
			}
			else if(choice == 2) {
				cart.clear();
				System.out.println("Thanks for trying. Come back soon, next week is holiday sales!");
				finished = true;
			}
			else if(choice == 3) {
				finished = false;
			}
			else {
				System.out.println("Enter valid number next time.. ");
			}
		}
		catch(InputMismatchException e) {
			System.out.println("Enter  number Next time.. ");
		}
		return finished;
	}
	
	
	// uses other methods to calculate and display information about the order
	private void showBill(String user) {
		
		int cus_id = getCusId(user);
		
		double total = getTotal();

		double bonus = getBonus(user);
		

		double payment = total - bonus;

		System.out.println(cart.size()>0 ? "This is you Cart:\n"+this.toString() : "Cart is empty. Go add some products to it...");
		System.out.println("This is The Total: "+ total+ " $");
		System.out.println("This is your account discount: "+ bonus+ " $");

		if(payment < 0) {
			CoffeZone.setCustomerDiscount(cus_id, -1*total);
			System.out.println("Final payment : "+ 0 + " $");
			System.out.println("Enjoy your beverage. Happy Christmas ");
		}
		else {
			CoffeZone.setCustomerDiscount(cus_id, -1*bonus);
			System.out.println("Final payment : "+ payment + " $");
			System.out.println("Enjoy your beverage. Happy Christmas ");
		}
	
	}
//Create a new order and returns the new primary key
	 private boolean createOrder(String user,String addrs) { 
			
		 	boolean created = false;
		 	String q1 = "{ call ordering.create_sale(?,?,?)}";
		 	String q2 = "{ call ordering.add_product_sales(?,?,?)}";
			CallableStatement cstm = null;
			try {
				CoffeZone.conn.setAutoCommit(false);
				cstm = CoffeZone.conn.prepareCall(q1);
				cstm.setString(1, user);
				cstm.registerOutParameter(2, Types.INTEGER);	
				cstm.setString(3, addrs);
				cstm.executeUpdate();
				int sale_id = cstm.getInt(2);
				for(SelectedProducts sp : cart) {
					cstm = CoffeZone.conn.prepareCall(q2);
					cstm.setInt(1, sp.getMenuNum());
					cstm.setInt(2, sale_id);	
					cstm.setInt(3, sp.getQuantity());
					cstm.executeUpdate();
				}
				CoffeZone.conn.commit();
				created = true;
			} catch (SQLException e) {
				try {
					CoffeZone.conn.rollback();
				} catch (SQLException e1) {
					System.out.println("Connection problems ....");
				}
				System.out.println("Sorry failed to order!");
			}
			finally {
			    if (cstm != null) {
			    	try {
			    		cstm.close();
					} catch (SQLException e) {
						created = false;
						System.out.println("we are having problems with the database Connection...");
					}
			    }
			}
			return created;
	 }
	 


	/**
	 * use prepared statament to get the current address
	 * @param user
	 * @return customer's address
	 */
	private String getAddress(String user) {
		int cus = getCusId( user);
		String q = "select address from clients where cus_id = ? ";
		PreparedStatement ps = null;
		try {
			ps = CoffeZone.conn.prepareStatement(q);
			ps.setInt(1,cus);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getString("address");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
		    if (ps != null) {
		    	try {
		    		ps.close();
				} catch (SQLException e) {
					System.out.println("we are having problems with the database Connection...");
				}
		    }
		}

		return null;

	}

	/**
	 * ask user if wants to change shipping address
	 * @param addr
	 * @return shipping address
	 */
	private String shippingAddress(String addr) {

		Scanner reader = new Scanner(System.in);
		boolean valid = false;
		String newAdd = addr;
		while(!valid) {
			try {
				System.out.println("This is your Current shipping address \n "+
						addr
						);
				System.out.println("Do you want to change it? Enter 1 if yes. Enter 2 if no");
				int choice = reader.nextInt();
				if(choice == 1 ) {
					if(newAdd!=null || newAdd!="") {
						newAdd = getNewAdd();
					}

					valid = true;
				}
				else if(choice == 2) {
					valid = true;
				}
				else {
					System.out.println("enter a valid number");
				}
			}
			catch(InputMismatchException e) {
				System.out.println("enter a number");
			}
		}

		return newAdd;
	}

	/**
	 * ask user for new address
	 * @return new address
	 */
	private String getNewAdd() {
		Scanner r = new Scanner(System.in);
		System.out.println("Enter your shipping address");
		String newAdd =r.nextLine();
		return newAdd;
	}

	/**
	 * get the discount the user has on the new sales.
	 * @param user
	 * @return discount
	 */
	public double getBonus(String user) {
		
		int cus_id = getCusId(user);
		double bonus = 0;
		String q = "select discount from clients where cus_id = ?";
		
		PreparedStatement ps = null;
		try {
			ps = CoffeZone.conn.prepareStatement(q);
			ps.setInt(1, cus_id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				bonus = rs.getDouble("discount");
				
			} 
		}
		catch (SQLException e) {
			System.out.println("No dicount Today");
		}
		finally {
		    if (ps != null) {
		    	try {
		    		ps.close();
				} catch (SQLException e) {
					System.out.println("we are having problems with the database Connection...");
				}
		    }
		}
		
		return bonus;

	}

	/**
	 * use collable statement to get the customer_id
	 * @param user
	 * @return customer_id
	 */

	public int getCusId(String user) {

		int cusid = 0;
		String q = "{? = call customer_authentication.clientId(?)}";
		CallableStatement cstm = null;
		try {
			cstm = CoffeZone.conn.prepareCall(q);
			cstm.registerOutParameter(1, Types.INTEGER);
			cstm.setString(2, user);
			cstm.executeUpdate();
			cusid = cstm.getInt(1);
		} catch (SQLException e) {
			System.out.println("No such customerId");
		}
		finally {
		    if (cstm != null) {
		    	try {
		    		cstm.close();
				} catch (SQLException e) {
					System.out.println("we are having problems with the database Connection...");
				}
		    }
		}
		return cusid;
	}
	/**
	 * Use collable statement to get the number of
	 * refferals the customer has
	 * @param customer_id
	 * @return the count of referrals
	 */
	public int getReferralCount( int customer_id) {
		int count = 0;
		String q = "{? = call customer_authentication.referral_count(?)}";
		CallableStatement cstm;
		try {
			cstm = CoffeZone.conn.prepareCall(q);
			cstm.registerOutParameter(1, Types.INTEGER);
			cstm.setInt(2, customer_id);
			cstm.executeUpdate();
			count = cstm.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	/**
	 * Calculate the total price of the cart
	 * @return total price
	 */
	public double getTotal() {

		double total = 0;

		for(SelectedProducts sp : cart) {
			total+=sp.getPrice()*sp.getQuantity();
		}

		return total;
	}

	@Override
	public String toString() {

		StringBuilder sb= new StringBuilder();

		for(SelectedProducts sp : cart) {
			sb.append(sp.toString()+"\n");
		}

		return sb.toString();
	}


	// return the product if it exist in the cart else null
	public SelectedProducts menuExist(int menuNum) {

		for(SelectedProducts sp : cart) {
			if(sp.getMenuNum() == menuNum) {
				return sp;
			}
		}
		return null;
	}

}