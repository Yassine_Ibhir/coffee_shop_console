package eclipse.shop;
/**
 * This class is the ingredients object
 * that will be stored in each product
 * @author Yassine Ibhir
 *
 */
public class Ingredients {
	private int ingr;
	private int quantity;

	//Constructor
	public Ingredients(int ingr,int quantity) {
		this.ingr = ingr;
		this.quantity = quantity;
	}

	//getters
	public int getIngr() {
		return this.ingr;
	}

	public int getQ() {
		return this.quantity;
	}
}