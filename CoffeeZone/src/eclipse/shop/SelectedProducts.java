package eclipse.shop;

import java.util.ArrayList;

/**
 * Class stores the selectedProduct in an arrayList.
 * It extends the product class
 * @author Yassine Ibhir
 *
 */
public class SelectedProducts extends Product{

	private int quantity;
	private ArrayList<Ingredients> ingreds = new ArrayList<Ingredients>();
	
	//Constructor
	public SelectedProducts(int menuNum, String name, double price, int quantity,ArrayList<Ingredients> ingreds ) {
		super(menuNum, name, price,ingreds);
		this.quantity = quantity;

	}
	

	// getters

	public int getQuantity() {
		return this.quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return super.toString()+" Quantity : "+ quantity;
	}

}
