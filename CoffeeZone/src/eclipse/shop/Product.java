package coffee;

import java.util.ArrayList;

import eclipse.main.CoffeZone;

/**
 * This class contains the product object
 * It manages the changes happens while customer
 *	adding or modifing products
 * @author Yassine Ibhir
 *
 */
public class Product {

	private int menuNum;
	private String name;
	private double price;
	private ArrayList<Ingredients> ingreds = new ArrayList<Ingredients>();


	public Product(int menuNum, String name, double price,ArrayList<Ingredients> ingreds) {
		this.menuNum = menuNum;
		this.name = name;
		this.price = price;
		this.ingreds = ingreds;
	}

	//getters
	public int getMenuNum() {
		return this.menuNum;
	}

	public String getName() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}
	
	public ArrayList<Ingredients> getIngredients() {
		return this.ingreds;
	}
	
	/**
	 * method checks if product can be sold.
	 * it calls a method in Stock class that 
	 * changes the value of the ingredient stock.
	 * @param quantity
	 * @return true if product can be sold
	 */
	public boolean isProductAvailable(int quantity) {
		
		for(Ingredients ing : this.ingreds) {
			int stock = getStock(ing.getIngr());
			if(ing.getQ()*quantity>stock) {
				return false;
			}
		}
		// change the value of the state..
		Stock.changeStockState(quantity,this.ingreds);
		return true;
	}
	
	/**
	 * helper method of isProductAvailable.
	 * @param ing_id ingredient id
	 * @return the stock quantity relative to ingredient
	 */
	private int getStock(int ing_id) {
		
		ArrayList<Stock> ingStock = CoffeZone.ingr_stock;
		
		int stock = 0;
		for(Stock s : ingStock) {
			if(ing_id == s.getIngredient()) {
				
				stock =  s.getStock();
			}
		}
		
		return stock;
	}
	
	@Override
	public String toString() {
		return "Menu# : "+  menuNum + ". Product : "+  name + " .Price: "+  price ;
	}

}
