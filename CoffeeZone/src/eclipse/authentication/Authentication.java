package eclipse.authentication;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import eclipse.main.CoffeZone;

public class Authentication {

	private String userName;
	private String psw;
	private String referralUserName;
	private String address;

	//length of each  field must be greater than or equal to 5
	private static final int lField = 5;
	// random object
	private static SecureRandom random = new SecureRandom();

	// Constructor for registration
	public Authentication(String userName,
			String psw,String referralUserName,
			String address) {
		if(userName != null && psw != null  && address != null && referralUserName!=null) {

			if(userName.length() >= lField && psw.length() >= lField && address.length() >= lField) {

				this.userName = userName;
				this.psw = psw;
				this.referralUserName = referralUserName;
				this.address = address;
			}
			else {
				throw new IllegalArgumentException("invalid inputs");
			}	
		}
		else {
			throw new IllegalArgumentException("invalid inputs");
		}
	}

	//Constructor for login
	public Authentication(String userName,String psw) {
		this.userName = userName;
		this.psw = psw;
	}

	public String getAddress() {
		return this.address;
	}

	public String getUname() {
		return this.userName;
	}
	public String getPassword() {
		return this.psw;
	}

	/**
	 * Method calls a pl/ssql procedure.
	 * it stores all the necessary values for
	 * a customer to register in the clients table.
	 * @return true if it suucefully added the customer else returns false
	 */
	public boolean addNewCustomer() {

		String salt = getSalt();

		byte [] hashed = hash(this.psw,salt);

		CallableStatement cst = null;

		String  sql = "{ CALL customer_authentication.add_client"
				+ "(?,?,?,?,?) }";
		try {

			cst = CoffeZone.conn.prepareCall(sql);
			cst.setString(1, this.userName);
			cst.setBytes(2,hashed);
			cst.setString(3,salt);
			cst.setString(4,this.address);
			cst.setString(5,this.referralUserName);
			cst.execute();
		}
		catch(SQLException e) {
			return false;
		}
	
		finally {
	    if (cst != null) {
	    	try {
				cst.close();
			} catch (SQLException e) {
				return false;
			}
	    	}
	}

		return true;
	}


	/**
	 *  Checks if user entered valid username and password.
	 *  it Uses aprepared statement to get the psw and username
	 *  and compare them to the values of the login object.
	 * @return true if matches
	 */
	public boolean isLogin() {

		String q = "SELECT * FROM clients WHERE user_name = ?";
		byte[] hashed = null;
		String salted = null;
		PreparedStatement ps = null;
		try {
			 ps = CoffeZone.conn.prepareStatement(q);
			ps.setString(1, this.userName);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				hashed = rs.getBytes("password");
				salted = rs.getString("salt");
			}
			else {
				return false;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		finally {
		    if (ps != null) {
		    	try {
					ps.close();
				} catch (SQLException e) {
					return false;
				}
		    }
		}

		byte [] com = hash(this.psw,salted);

		return Arrays.equals(com,hashed);

	}

	/**
	 *  adds the login time with username in the logs table
	 */
	public void addLogInfo() {

		CallableStatement cst = null ;
		String  sql = "{ CALL customer_authentication.add_logs"
				+ "(?) }";
		try {

			cst = CoffeZone.conn.prepareCall(sql);
			cst.setString(1, this.userName);
			cst.execute();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
		    if (cst != null) {
		    	try {
					cst.close();
				} catch (SQLException e) {
					System.out.println("connection is not closed..");
				}
		    }
		}	
	}


	//Creates a randomly generated String
	private String getSalt(){
		return new BigInteger(140, random).toString(32);
	}

	//Takes a password and a salt a performs a one way hashing on them, returning an array of bytes.
	public byte[] hash(String password, String salt){
		try{
			SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );

			/*When defining the keyspec, in addition to passing in the password and salt, we also pass in
				a number of iterations (1024) and a key size (256). The number of iterations, 1024, is the
				number of times we perform our hashing function on the input. Normally, you could increase security
				further by using a different number of iterations for each user (in the same way you use a different
				salt for each user) and storing that number of iterations. Here, we just use a constant number of
				iterations. The key size is the number of bits we want in the output hash*/ 
			PBEKeySpec spec = new PBEKeySpec( password.toCharArray(), salt.getBytes(), 1024, 256 );

			SecretKey key = skf.generateSecret( spec );
			byte[] hash = key.getEncoded( );
			return hash;
		}catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
			throw new RuntimeException( e );
		}
	}
}
