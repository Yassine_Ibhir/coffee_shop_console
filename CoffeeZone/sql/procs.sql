-- Yassine Ibhir

-- 1) package contains all functionalities of registration and login process.
CREATE OR REPLACE PACKAGE customer_authentication AS
    
    -- This exception will be raised when customer referred 3 clients
    customer_cant_refer  EXCEPTION;
    -- PROCEDURE sets the discount for a customer that refers.
    PROCEDURE set_bonus(referid IN number,amount in number);
    
    -- Function will count how many clients a customer has referred.
    FUNCTION referral_count (
        referid IN number
    ) RETURN NUMBER;

    -- Function will return the client_id if he can refer otherwise return -1.
    FUNCTION can_refer (
        referid IN NUMBER
    ) RETURN NUMBER;
    
    -- Function will return the client id. it takes the userName as input.
    FUNCTION clientId(
        username IN varchar2
    ) RETURN NUMBER;
    
    -- This procedure will use the fuctions above and queries to add a client to the databse. 
    PROCEDURE add_client(
          username IN varchar2,
          hashed IN RAW,
          salted IN varchar2,
          addrs varchar2,
          refer varchar2);
    
    -- procedure add log time into the logs table
     PROCEDURE add_logs(
          username IN varchar2
          );      

END  customer_authentication ;

-- implement package body

CREATE OR REPLACE PACKAGE BODY customer_authentication AS

  -- PROCEDURE sets the discount for a customer that refers.
    PROCEDURE set_bonus(referid IN number, amount in number) as
    
    current_bonus number;
    
    begin
        select discount into current_bonus from clients where cus_id = referid;
        
        update clients set discount=current_bonus+amount where cus_id=referid ;
    end;

-- Function will count how many clients a customer has referred.
    FUNCTION referral_count (
        referid IN number
    ) RETURN NUMBER AS
        counts number;
    BEGIN
     
     SELECT COUNT(REFERRAL) into counts FROM CLIENTS WHERE REFERRAL = referid; 
     
     return counts;
     
    EXCEPTION
    when others then
    return 0;
    
    END;
    
    -- Function will return the client_id if he can refer otherwise return -1.
    FUNCTION can_refer (
        referid IN NUMBER
    ) RETURN NUMBER AS
        counts NUMBER;
        
    BEGIN
    
    counts := referral_count (referid);
    
    if counts < 3 then
        return referid;
    end if;
    
    raise customer_cant_refer;
 
    EXCEPTION 
    when customer_cant_refer  then
    return -1;
    END;
    
     -- Function will return the client id or -1 if customer does not exist. it takes the userName as input
    FUNCTION clientId(
        username IN varchar2
    ) RETURN NUMBER AS
    
    referid number;
    
    BEGIN
    
    SELECT cus_id INTO referid FROM CLIENTS WHERE user_name = username;
    
    return referid;
    
    EXCEPTION
        when others then
        return -1;
       
    END;
    
-- This procedure will use the fuctions above and queries to add a client to the databse.
 
    PROCEDURE add_client(
          username IN varchar2,
          hashed IN RAW,
          salted IN varchar2,
          addrs varchar2,
          refer varchar2) AS
          
          
          client_id number;
          referid number;
          new_cus_id number;
          
          begin
          
          -- method returns client id or -1 if client does not exist
          client_id  := clientId(refer);
          
          -- new primary key for new client
          select max(cus_id)+1 into new_cus_id  from clients;
          
          if client_id  = -1 then
          
           -- no referred customer 
                    insert into clients  values ( new_cus_id,username,hashed,salted,addrs,null,0);
          
          else
                 -- method returns client id if he can refer otherwise retuns -1
                    referid := can_refer(client_id);
                    
                    if referid = -1 then
                    
                        insert into clients values ( new_cus_id,username,hashed,salted,addrs,null,0);
                     
                    else
                   
                        insert into clients values(new_cus_id,username,hashed,salted,addrs,referid,0);
                    
                    end if;
                    
           end if;         

          
          end;
          
          -- procedure add log time into the logs table
     PROCEDURE add_logs(
          username IN varchar2
          ) As 
          
          begin 
          
          -- the format of the date in my db includes the time.
          insert into logs values (username,current_date);
          
          end;
       

END customer_authentication;



-- 2) package contains all functionalities of ordering.
CREATE OR REPLACE PACKAGE ordering AS
 
    -- procedure  creates a new sale. 
    PROCEDURE  create_sale(uname in varchar2, sale_id out number, ship_address in varchar2) ; 
    
    -- add the cart to the bridging table
    PROCEDURE add_product_sales(productId in number, sale_id in number,quantity in number) ;

END ordering ;

--implement package body

CREATE OR REPLACE PACKAGE  BODY ordering AS
    
    -- procedure will add a new sale to the sales table. It uses package of autentication to get the customerid.
    PROCEDURE  create_sale(uname in varchar2, sale_id out number, ship_address in varchar2) AS
        
        new_sale_id number;
        
        client_id number;
        
        begin
        
        select  max(order_num)+1 into new_sale_id from sales;
        
        client_id := customer_authentication.clientid(uname);
        
        if client_id != -1 then
        
            insert into sales values(new_sale_id,client_id,ship_address,current_date+1,current_date);
            
            -- out paramater will be used in bridging table.
            sale_id := new_sale_id;
        
        end if;
   
        end;
        
        -- procedure inserto values into bridging table.
        PROCEDURE add_product_sales(productId in number, sale_id in number,quantity in number) AS
        
        BEGIN
        
        INSERT into product_sales values (sale_id,productId,quantity);
        
        EXCEPTION
        WHEN OTHERS THEN
        IF sqlcode = -20005 THEN
            dbms_output.put_line('Not enough ingredients');
        END IF;
        END;
            
END ordering;

-- 3) -- 2) package has methods related to ingredients management and orders.
CREATE OR REPLACE PACKAGE productIngredients AS

-- Associative array to store ingredientId and quantity
    TYPE ingr_quantity 
        IS TABLE OF number(10) 
        INDEX BY PLS_INTEGER;
    
-- cursor has the ingredients and their quantities for a product
--cursor will be used getArrayIng 
    FUNCTION getArrayIng(menu_id in number) return ingr_quantity;

-- function returns the stock of an ingredient
    FUNCTION get_stock(ingr in number) return number;
-- function checks if the quantity in stock is sufficient to make the product. returns yes or no.
    FUNCTION isSufficient(product_id in number, quantity in number) return CHAR;

--  FUNCTION return the quantity used left in the stock after the product is sold.
    PROCEDURE update_stock(menu_id number,quant number) ;

end  productIngredients;

-- Implement the package
CREATE OR REPLACE PACKAGE  BODY productIngredients AS
    
        PROCEDURE update_stock(menu_id number,quant number) as
        ingrQ ingr_quantity := getArrayIng(menu_id);
        stocks number;
        ki PLS_INTEGER;
        
        
        begin
          
          ki :=  ingrQ.first;
        
        while ki is not null loop
        
                stocks := get_stock(ki)-quant*ingrQ(ki);
    
                update ingredients set stock = stocks where ingr_id = ki;   
            
                ki :=  ingrQ.next(ki);
        end loop;   
        
        end;
    
    FUNCTION getArrayIng(menu_id in number) return ingr_quantity as 
    
    ingredient number(4,0);
    quant number(4,0);
    pi_array ingr_quantity;
      
    cursor p_ingredients is 
        select ingr_id,ingr_quantity
            from product_ingredients
            where product_id = menu_id;
            
    begin
            
            open p_ingredients;
          
         loop
         
         fetch  p_ingredients into ingredient,quant;
         
         exit when p_ingredients%NOTFOUND;
         
         pi_array(ingredient):= quant;
         
         end loop;
         
         return pi_array;
            
    end;
    
   -- function returns the stock quantity of an ingredient
    FUNCTION get_stock(ingr in number) return number as
    
    stock_left number;
    
    begin
    select stock into stock_left 
        from ingredients where ingr_id = ingr;
        
        return stock_left;
        
Exception
    when NO_DATA_FOUND THEN
    RETURN 0;
    
    end;
    
    -- function checks if the quantity in stock is sufficient to make the product. returns yes or no.
    FUNCTION isSufficient(product_id in number, quantity in number) return CHAR AS
    
      ingrQ ingr_quantity := getArrayIng(product_id);
      ki PLS_INTEGER;
      stocks number;
      result char(1) := 'y';
      
      begin
      
        ki :=  ingrQ.first;
        
        while ki is not null loop
        
                stocks := get_stock(ki);
                
                if(quantity*ingrQ(ki) > stocks) then
                     return 'n';
                end if;     
            
                ki :=  ingrQ.next(ki);
        end loop;
        
        return result;
        exception
        when others then
        return 'n';
        end;
    
END productIngredients;

/**
Trigger will use  procedures and functions inside 
productIngredients package to check if a product has
enough ingredients before inserting to the product_sale
table (This is not necessary though because java
has the stock object that takes care of that)  but 
I decided to leave the productIngredients  
package in case I need it in my future work.
*/

CREATE OR REPLACE TRIGGER stock_trig BEFORE
    INSERT ON product_sales
    FOR EACH ROW
    DECLARE
    rslt char(1);
    reject_exc EXCEPTION;
    PRAGMA exception_init ( reject_exc, -20005 );
BEGIN
    rslt := productIngredients.isSufficient(:new.product_id,:new.product_quantity);
    if(rslt = 'n') then
    raise_application_error(-20005, 'Not enough Ingredients');
    else
    productingredients.update_stock(:new.product_id,:new.product_quantity);
    end if;
        
END;

