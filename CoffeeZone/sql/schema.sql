--Yassine Ibhir

-- customer table
CREATE TABLE clients
    (cus_id number(4,0) primary key,
    user_name varchar2(50) not null,
    password RAW(50) not null,
    salt varchar2(50) not null,
    address varchar2(50) not null,
    referral number(4,0),
    discount number(4,0) default 0,
    CONSTRAINT FK_referred FOREIGN KEY (referral)
    REFERENCES clients(cus_id));
       
-- sales table
CREATE TABLE sales
    (order_num number(4,0) primary key,
    cus_id number(4,0),
    shipping_address varchar2(50),
    delivery_date date,
    placement_date date,
    CONSTRAINT FK_customer FOREIGN KEY (cus_id)
    REFERENCES clients(cus_id));
    
-- products table
CREATE TABLE products
    (product_id number(4,0) primary key,
    product_name varchar(50),
    price number(4,2) 
    );
    
-- index on products
CREATE INDEX products_i on products (product_id,product_name, price);
    
-- bridging table: product_sales
CREATE TABLE product_sales
    (order_num number(4,0),
    product_id number(4,0),
    product_quantity number(4,0),
    CONSTRAINT quantity_c CHECK (product_quantity> 0),
    CONSTRAINT FK_order FOREIGN KEY (order_num)
    REFERENCES sales(order_num),
    CONSTRAINT FK_product FOREIGN KEY (product_id)
    REFERENCES products(product_id));
 
-- ingredients table
CREATE TABLE ingredients
    (ingr_id number(4,0) primary key,
    ingr_name varchar(50),
    restock_price number(4,2),
    stock number(4,0)
    );
    
-- index on ingredients.   
CREATE INDEX ingredients_i on ingredients (ingr_id,ingr_name);    

-- bridging table: product_incredients.    
CREATE TABLE product_ingredients
    (product_id number(4,0),
    ingr_id number(4,0),
    ingr_quantity number(4,0),
    CONSTRAINT FK_prod FOREIGN KEY (product_id)
    REFERENCES products(product_id),
    CONSTRAINT FK_ingr FOREIGN KEY (ingr_id)
    REFERENCES ingredients(ingr_id));
    
-- index on ingredients.   
CREATE INDEX producIngr_i on product_ingredients (ingr_id,product_id,ingr_quantity); 

-- customer log table
CREATE TABLE logs
    (cus_uname varchar2(50) not null,
    log_date date not null);
    

-- Delete all tables and indexes
drop table logs purge;
drop index producIngr_i;    
drop table product_ingredients purge; 
drop index ingredients_i;
drop table ingredients purge;
drop table product_sales purge;
drop index products_i;
drop table products purge;    
drop table sales purge; 
drop table clients purge; 
   