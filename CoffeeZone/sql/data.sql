--Yassine Ibhir

--Test data for clients table
INSERT INTO clients (
    cus_id,
    user_name,
    password,
    salt,
    address,
    referral
) VALUES (
    1,
    'byoude0',
    '70457baba06b26c16e0de099e88c8b238a0818c5',
    'salt1',
    '1914 Victoria Crossing',
    NULL
);

INSERT INTO clients (
    cus_id,
    user_name,
    password,
    salt,
    address,
    referral
) VALUES (
    2,
    'jinch1',
    '70457baba06b26c16e0de099e88c8b238a0818c5',
    'salt2',
    '6 Tennessee Road',
    1
);

INSERT INTO clients (
    cus_id,
    user_name,
    password,
    salt,
    address,
    referral
) VALUES (
    3,
    'frimer2',
    '70457baba06b26c16e0de099e88c8b238a0818c5',
    'salt3',
    '494 Nevada Point',
    2
);

--Test data for sales

INSERT INTO sales (
    order_num,
    cus_id,
    shipping_address,
    delivery_date,
    placement_date
) VALUES (
    1,
    3,
    '557 Oneill Road',
    '10/DEC/2020',
    '9/DEC/2020'
);

INSERT INTO sales (
    order_num,
    cus_id,
    shipping_address,
    delivery_date,
    placement_date
) VALUES (
    2,
    2,
    '34074 Texas Center',
    '11/DEC/2020',
    '14/DEC/2020'
);

-- Products Data

INSERT INTO products (
    product_id,
    product_name,
    price
) VALUES (
    1,
    'caramel latte',
    5
);

INSERT INTO products (
    product_id,
    product_name,
    price
) VALUES (
    2,
    'cafe au lait',
    3.66
);

INSERT INTO products (
    product_id,
    product_name,
    price
) VALUES (
    3,
    'sweet espresso',
    3.5
);

INSERT INTO products (
    product_id,
    product_name,
    price
) VALUES (
    4,
    'sweet coffee',
    2.99
);

--Ingredients data

INSERT INTO ingredients (
    ingr_id,
    ingr_name,
    restock_price,
    stock
) VALUES (
    1,
    'milk',
    0.5,
    20
);

INSERT INTO ingredients (
    ingr_id,
    ingr_name,
    restock_price,
    stock
) VALUES (
    2,
    'coffee beans',
    1,
    20
);

INSERT INTO ingredients (
    ingr_id,
    ingr_name,
    restock_price,
    stock
) VALUES (
    3,
    'caramel syrup',
    0.5,
    20
);

INSERT INTO ingredients (
    ingr_id,
    ingr_name,
    restock_price,
    stock
) VALUES (
    4,
    'sugar',
    0.2,
    20
);

-- product_ingredients : bridging table

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    1,
    1,
    2
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    1,
    3,
    1
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    1,
    2,
    2
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    2,
    2,
    3
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    2,
    1,
    2
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    2,
    4,
    2
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    3,
    2,
    3
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    3,
    4,
    2
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    4,
    2,
    2
);

INSERT INTO product_ingredients (
    product_id,
    ingr_id,
    ingr_quantity
) VALUES (
    4,
    4,
    2
);

-- Test data for product_sales

INSERT INTO product_sales (
    order_num,
    product_id,
    product_quantity
) VALUES (
    1,
    1,
    2
);

INSERT INTO product_sales (
    order_num,
    product_id,
    product_quantity
) VALUES (
    2,
    4,
    1
);

INSERT INTO product_sales (
    order_num,
    product_id,
    product_quantity
) VALUES (
    2,
    2,
    2
);